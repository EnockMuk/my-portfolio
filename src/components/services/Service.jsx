import React from 'react'
import './service.css'
import {BsFillPatchCheckFill} from 'react-icons/bs'
export default function Service() {
  return (
    <section id='service'>

      <h5>What I offer</h5>
      <h2>Services</h2>
      <div className="container services__container">
        <article className='service'>
          <div className="service__head">
            <h3>Web App</h3>
          </div>
          <ul className='service__list'>
            <li> 
              <BsFillPatchCheckFill className='service__list-icon'/>
              <p> Front-End Development</p>
              
            </li>
            
            <li> 
              <BsFillPatchCheckFill className='service__list-icon'/>
              
              <p>Back-End Development</p>
            </li>
           
          </ul>
        </article>

        <article className='service'>
          <div className="service__head">
            <h3> Technical Support</h3>
          </div>
          <ul  className='service__list'>

          <li> 
              <BsFillPatchCheckFill className='service__list-icon'/>
              <p>Monitor incidents until resolution</p>
               <p> Website Maintenance and Support </p>
            </li>
            <li> 
              <BsFillPatchCheckFill className='service__list-icon'/>
              <p>Diagnose and resolve hardware and software issues</p>
              
            </li>
            
            <li> 
              <BsFillPatchCheckFill className='service__list-icon'/>
              <p>
                Assign unresolved incidents  to the appropriate support team, 
                identify and escalate incidents requiring urgent attention and action
                
              </p>
              
            </li>
          
          </ul>
        </article>

        <article className='service'>
          <div className="service__head">
            <h3>Web3</h3>
          </div>
          <ul className='service__list'>
            <li> 
              <BsFillPatchCheckFill className='service__list-icon'/>
              <p>Smart Contract Development</p>
              
            </li>
            <li> 
              <BsFillPatchCheckFill className='service__list-icon'/>
              <p>Education and Training </p>
             
            </li>
            <li> 
              <BsFillPatchCheckFill className='service__list-icon'/>
              <p>Web3 Integration :  ether.js  / web3.js </p>
              
            </li>
            
          </ul>
        </article>

      </div>
    </section>
  )
}
