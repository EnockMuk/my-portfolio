import React from 'react'
import './portfolio.css'
import MaxFit from '../../assets/maxfit.png';
import MyPortfolio from '../../assets/myportfolio.png'
import Gorilla from '../../assets/gorilla.jpg'
import Lending from '../../assets/lending.png'
export default function Portfolio() {

  const data =[{
    id:1,
    image:MaxFit,
    title:"Maxfit",
    git:"https://gitlab.com/fitness930421/fitness-max",
    demo:"https://maxfit.com"
  },
  
  {
      id:2,
      image:Gorilla,
      title:"Collection Nfts",
      git:"https://github.com/EnockMuk/nft-collection",
      demo:""
   },
   {
    id:3,
    image:MyPortfolio,
    title:"My Portfolio",
    git:"https://gitlab.com/EnockMuk/my-portfolio",
    demo:""
 },
 {
  id:4,
  image:Lending,
  title:"Crypto Lending App",
  git:"https://github.com/EnockMuk/Lending-Token",
  demo:""
},


  ]


  return (
    <section id='portfolio'>
      <h5>My recent work</h5>
        <h2>Portfolio</h2>
        <div className="container portfolio__container">
          {data.map(({id,image,title,git,demo})=>{
            return(
              <article key={id} className='portfolio__item'>
            <div className="portfolio__item-image">
                <img src={image} alt={title}/>
            </div>
            <h3>{title}</h3>
            <div className="portfolio__item-cta">
            <a href={git} className='btn' target='_blank'>Git</a>
            {/* <a href={demo} className='btn btn-primary' target='_blank'>Live Demo</a> */}
            </div>  
          </article>
            )
          }
          )
          }
          
        </div>

    </section>
  )
}
