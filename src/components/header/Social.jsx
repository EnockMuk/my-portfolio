import React from 'react'
import {BsLinkedin} from 'react-icons/bs'
import {AiFillGitlab} from 'react-icons/ai'
import {FaTwitter} from 'react-icons/fa'
export const Social = () => {
  return (
    <div className="header__socials">
        <a href="https://www.linkedin.com/in/enock-mukolos-36699519a/" target='_blanck'><BsLinkedin/> </a>
        <a href="https://gitlab.com/EnockMuk" target='_blank'><AiFillGitlab/></a>
        <a href="https://twitter.com/wilondja_enock"><FaTwitter/></a>
    </div>
  )
}
