import React from 'react'
import myResume from '../../assets/web.docx';
export const Resume = () => {
  return (
    <div className="rsm">
        <a href={myResume} download className='btn'>Download Resume </a>
        <a href="#contact" className='btn btn-primary'> Let's Talk </a>
    </div>
  )
}
