import React from 'react'
import './header.css'
import { Resume } from './Resume'
import me from '../../assets/enockphoto2.png'
import { Social } from './Social'
export default function Header() {
  return (
    <header>
      <div className="container header__container">
        <h5>Hello, I'm Enock</h5>
        <h1>Junior Developer</h1>
        <h5 className='text-light'>Fullstack Developer</h5>
        <Resume/>
        <Social/>
        <div className="me">
          <img src={me} alt="" />
        </div>
        <a href="#contact" className='scroll__down'>Scroll Down</a>
      </div>
    </header>
  )
}
