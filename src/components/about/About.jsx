import React from 'react'
import './about.css'
import meee from '../../assets/meee.png'
import {TbAwardFilled} from 'react-icons/tb'
import {FaUserSecret} from 'react-icons/fa'
import {SiCodeproject} from 'react-icons/si'
export default function About() {
  return (
    <section id='about'>
      <h5> Enock's info</h5>
      <h2>About Mukolondjolo</h2>
      <div className="container about__container">
        <div className="about__me">
          <div className="about__me-image">
            <img src={meee} alt="enock image" />
          </div>
        </div>
        <div className="about__content">
          <div className="about__cards">
            <article className="about__card">
              <TbAwardFilled className="about_icon"/>
              <h5>Experience</h5>
              <small>1 year </small>
            </article>

            <article className="about__card">
              <FaUserSecret className="about__icon"/>
              <h5>Clients</h5>
              <small> 1 </small>
            </article>

            <article className="about__card">
              <SiCodeproject className="about__icon"/>
              <h5>Projects</h5>
              <small>3 completed </small>
            </article>
          </div>

          <p>
          Passionate blockchain and web developer dedicated to creating innovative solutions. 
          Proficient in JavaScript, HTML, CSS, and experienced with blockchain platforms like Ethereum, Polygon and Bitcoin. 
          Excited to contribute to decentralized applications and explore the potential of blockchain technology. 
          Recent graduate with a strong foundation in smart contract development and web development frameworks such as REACT. 
          Open to collaboration and eager to be a part of projects that drive innovation.
          </p>
          <a href="#contact" className='btn btn-primary'> Let's talk</a>
        </div>

      </div>
    </section>
  )
}
