import React from 'react'
import './experience.css'
import {SiHtml5} from 'react-icons/si'
import {GrReactjs} from 'react-icons/gr'
import {SiTypescript} from 'react-icons/si'
import {SiJavascript} from 'react-icons/si'
import {SiCsswizardry}from 'react-icons/si'
import {FaNodeJs} from 'react-icons/fa'
import {SiSolidity} from 'react-icons/si'
import {FaHardHat} from 'react-icons/fa'
import {DiMongodb} from 'react-icons/di'
import {TbBrandMysql} from 'react-icons/tb'
export default function Experience() {
  return (
    <section id='experience'>
      <h5>My Skills</h5>
      <h2>My Experience</h2>
      <div className="container experience__container">
      
        <div className="experience__frontend">
        <h3>Frontend development</h3>
              <div className="experience__content">
                
                <article className='experience_details'>
                  <SiHtml5 className='experience_details-icons'/>
                  <div>
                  <h4>HTML</h4>
                  <small className='text-light'>Experienced</small>
                  </div>
                 
                </article>

                <article className='experience_details'>
                  <SiCsswizardry className='experience_details-icons'/>
                  <div>
                  <h4>CSS</h4>
                  <small className='text-light'>Intermediate</small>
                  </div>
                 
                </article>
                <article className='experience_details'>
                  <GrReactjs className='experience_details-icons'/>
                  <div>
                  <h4>React</h4>
                  <small className='text-light'>beginner</small>
                  </div>
                  
                </article>

               

                <article className='experience_details'>
                  <SiJavascript className='experience_details-icons'/>
                  <div>
                  <h4>Javascript</h4>
                  <small className='text-light'>beginner</small>
                  </div>
                </article>
              </div>
        </div>
       
        <div className="experience__backend">
            <h3>Backend development</h3>
            
              <div className="experience__content">
                
                <article className='experience_details'>
                  <FaNodeJs className='experience_details-icons'/>
                  <div>
                  <h4>Nodejs</h4>
                  <small className='text-light'>Experienced</small>
                  </div>
                </article>

                <article className='experience_details'>
                <DiMongodb className='experience_details-icons'/>
                  <div>
                  <h4>MongoDb</h4>
                  <small className='text-light'>beginner</small>
                  </div>
                </article>
                <article className='experience_details'>
                  <SiSolidity className='experience_details-icons'/>
                  <div>
                  <h4>Solidity</h4>
                  <small className='text-light'>experienced</small>
                  </div>
                </article>

                <article className='experience_details'>
                  <SiTypescript className='experience_details-icons'/>
                  <div>
                  <h4>TypeScript</h4>
                  <small className='text-light'>beginner</small>
                  </div>
                </article>

                <article className='experience_details'>
                  <TbBrandMysql className='experience_details-icons'/>
                 <div>
                 <h4>Mysql</h4>
                  <small className='text-light'>beginner</small>
                 </div>
                </article>

                <article className='experience_details'>
                  <FaHardHat className='experience_details-icons'/>
                  <div>
                  <h4>Hardhat</h4>
                  <small className='text-light'>beginner</small>
                  </div>
                </article>
              </div>
        </div>

        </div>

    </section>
  )
}
