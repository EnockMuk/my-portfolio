import React from 'react'
import './footer.css'
import {SiTwitter} from 'react-icons/si'
import {FaLinkedin} from 'react-icons/fa'
import {ImYoutube} from 'react-icons/im'
export default function Footer() {
  return (
    <footer>
      <a href="" className='footer__logo'> EnockMukolos</a>
      <ul className='permalinks'>
        <li><a href="#">Home</a></li>
        <li><a href="#about">About</a></li>
        <li><a href="#experience">Experience</a></li>
        <li><a href="#service">Services</a></li>
        <li><a href="#portfolio">Portfolio</a></li>
        <li><a href="#testimonials">Testimonials</a></li>
        <li><a href="#contact">Contact</a></li>
      </ul>

      <div className="footer__socials">
        <a href="https://twitter.com/wilondja_enock"><SiTwitter/></a>
        <a href="https://www.linkedin.com/in/enock-mukolos-36699519a/"><FaLinkedin/></a>
        <a href="https://www.youtube.com/channel/UCXZ1UXTPASQrCIY9AC3PXdw"><ImYoutube/></a>
      </div>
      <div className="footer__copyright">
        <small>
          &copy; EnockMukolos cd. All rights reserved.
        </small>
      </div>
    </footer>
  )
}
