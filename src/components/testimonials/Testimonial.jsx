import React from 'react'
import './testimonial.css'
import client from '../../assets/meee.png'
import { Pagination } from 'swiper'
import {Swiper,SwiperSlide} from 'swiper/react'
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';


export default function Testimonial() {
  const data =[{
    id:1,
    avatar:client,
    name:"Gabin",
    review:" C'est fut un plaisir de travailler avec enock au collège"
  },
  
  ]
  return (
    <section id='testimonials'>
      <h5>Review from colleagues</h5>
      <h2> Testimonials</h2>

      <Swiper className="container testimonials__container"

      modules={[Pagination]}
      spaceBetween={40}
      slidesPerView={1}
      pagination={{ clickable: true }}
    >

        {
          data.map(({avatar,name,review},index)=>{
            return (
              <SwiperSlide className='testimonial' key={index}>
                <div className="client__avatar">
                  <img src={avatar} alt={name} />
                </div>
                <h5 className='client__name'>{name}</h5>
                <small className='client_review'>
                  {review}
                </small>

              </SwiperSlide>
            )
          })
        }
      </Swiper>

    </section>
  )
}
