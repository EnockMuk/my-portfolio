import React, { useState } from 'react'
import './nav.css'
import {FcHome} from 'react-icons/fc'
import {FcAbout} from 'react-icons/fc'
import {MdOutlineWorkHistory} from 'react-icons/md'
import {GrServices} from 'react-icons/gr'
import {FcBusinessContact} from 'react-icons/fc'

export default function Nav() {
  const [active,setActive]=useState('#')
  return (
    <nav>
      <a href="#" className={active==='#'?'active':''} onClick={()=>setActive('#')}><FcHome/></a>
      <a href="#about" className={active==='#about'?'#about':''} onClick={()=>setActive('#about')}><FcAbout/></a>
      <a href="#experience" className={active==='#experience'?'#experience':''} onClick={()=>setActive('#experience')}><MdOutlineWorkHistory/></a>
      <a href="#service" className={active==='#services'?'#services':''} onClick={()=>setActive('#service')}><GrServices/></a>
      <a href="#contact" className={active==='#contact'?'#contact':''} onClick={()=>setActive('#contact')}><FcBusinessContact/></a>
    </nav>
  )
}
