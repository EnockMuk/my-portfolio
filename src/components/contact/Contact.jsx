import React from 'react'
import './contact.css'
import {TfiEmail} from 'react-icons/tfi'
import {SiTwitter} from 'react-icons/si'
import {FaLinkedin} from 'react-icons/fa'
import {BsWhatsapp} from 'react-icons/bs'
import { useRef } from 'react'
import emailjs from 'emailjs-com'

export default function Contact() {
  const form=useRef();
  const sendEMail=(e)=>{
    e.preventDefault();
    emailjs.sendForm('service_d1w92ca','template_qm9q8d7',form.current,'mnQMfwlXRJmN1ypeb')
    .then((result) => {
      console.log(result.text);
  }, (error) => {
      console.log(error.text);
  });
  e.target.reset()
  }
  return (
    <section id='contact'>
      <h5>Get in touch</h5>
      <h2>Contact </h2>
      <div className="container contact__container">

        <div className="contact_options">

          <article className='contact__option'>
            <TfiEmail className='contact__option-icon'/>
            <h4>Email</h4>
            <h5>wenet90@gmail.com</h5>
            <a href="mailto:enockmukolondjolo@gmail.com">Send a message</a>
          </article>

          <article className='contact__option'>
            <SiTwitter className='contact__option-icon'/>
            <h4>Twitter </h4>
            <h5>@wilondja_enock</h5>
            <a href="https://twitter.com/wilondja_enock" target='_blank'>I'm 24 online on twitter</a>
          </article>

          <article className='contact__option'>
            <FaLinkedin className='contact__option-icon'/>
            <h4>Linkedin </h4>
            <h5>Enock Mukolos</h5>
            <a href="https://www.linkedin.com/in/enock-mukolos-36699519a/" target='_blank'>I'm 24/7 online on linkedin</a>
          </article>

          <article className='contact__option'>
            <BsWhatsapp className='contact__option-icon'  />
            <h4>Whatsap </h4>
            <h5>+ 1 </h5>
            <a href="https://api.whatsapp.com/send?phone=+13437770638" target='_blank'> Let's talk on whatsap</a>
          </article>

        </div>

          <form ref={form} onSubmit={sendEMail}>
            <input type="text" name='name' placeholder='your name' required/>
            <input type="email" name='email' placeholder='your email' required />
            <textarea name="message" placeholder='your message'   rows="7" required></textarea>
            <button type='submit' className='btn btn-primary'>Send your message</button>
          </form>
      </div>
    </section>
  )
}
